const http = require('http');
const express = require('express');
const { ExpressPeerServer } = require('peer');
// const fs = require('fs');

const port = process.env.PORT || 5000;
const app = express();

// const server = https.createServer({
// key: fs.readFileSync('./server.key'), cert: fs.readFileSync('./server.cert')
//  }, app);
const server = http.createServer({}, app);

const customGenerationFunction = () => (`${Math.random().toString(36)}`).substr(2, 16);

const peerServer = ExpressPeerServer(server, {
  port,
  path: '/',
  generateClientId: customGenerationFunction,
  // ssl: {
  //   key: fs.readFileSync('./server.key'),
  //   cert: fs.readFileSync('./server.cert'),
  // },
});

peerServer.on('connection', (client) => {
  console.log(client.id);
});

app.use('/', express.static('dist'));
app.use('/peerjs', peerServer);

server.listen(port);
