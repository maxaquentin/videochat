const fs = require('fs');
const { PeerServer } = require('peer');
const genId = require('./gen_id');

PeerServer({
  port: 9000,
  generateClientId: genId,
  ssl: {
    key: fs.readFileSync('./server.key'),
    cert: fs.readFileSync('./server.cert'),
  },
});
